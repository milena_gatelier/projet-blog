import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../model/post';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() post: Post;

  constructor() { }

  ngOnInit() {
  }

  onLike() {
    this.post.like++;
  };

  onUnlike() {
    this.post.unlike++;
  };

  getLike() {
    return this.post.like;
  }

  getUnlike() {
    return this.post.unlike;
  }

  getColor() {
    if (this.getLike() > this.getUnlike()) {
      return "green";
    } else if (this.getUnlike() > this.getLike()) {
      return "red";
    } else if (this.getUnlike() === this.getLike()) {
      return "black";
    }
  }

}
