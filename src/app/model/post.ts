import { Injectable } from '@angular/core';

export interface Post { 
    name: string;
    contenu: string;
    created_at: Date;
    like: number;
    unlike: number;
}